import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import streamReducer from './streamReducer';

import AuthGoogleReducer from './AuthGoogleReducer';

export default combineReducers({
  auth: AuthGoogleReducer,
  form: formReducer,
  streams: streamReducer
});