import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import StreamCreate from './streams/StreamCreate';
import StreamEdit from './streams/StreamEdit';
import StreamList from './streams/StreamList';
import StreamDelete from './streams/StreamDelete';
import StreamShow from './streams/StreamShow';
import Header from './Header';
import history from '../history';
/**
 * Chủ động tạo History nhằm sử dụng trong action, component (ko có router như modal, header, footer...) --> redirect, 
 * thông thường React-router-dom mặc đinh tạo history cho Component (action ko có)
 */

const App = () => {
  return (
    <div>
      <Router history={history}>
        <div className='ui container'>
          <Header />
          <Switch>
            <Route path='/' exact component={StreamList} />
            <Route path='/streams/new' component={StreamCreate} />
            <Route path='/streams/edit/:id' component={StreamEdit} />
            <Route path='/streams/delete/:id' component={StreamDelete} />
            <Route path='/streams/:id' component={StreamShow} />
          </Switch>
        </div>
      </Router>
    </div>
  )
}

export default App;