import React from 'react'
import { connect } from 'react-redux';
import { fetchStream, editStream } from '../../actions';
import StreamForm from './StreamForm';
import _ from 'lodash';

class StreamEdit extends React.Component {
  componentDidMount = () => {
    this.props.fetchStream(this.props.match.params.id)
  }

  onSubmit = (formValue) => {
    this.props.editStream(this.props.stream.id, formValue);
  }

  render() {
    if (!this.props.stream) {
      return (<div>Loading...</div>)
    } else if (this.props.stream && this.props.stream.userId !== this.props.currentUserId) {
      this.props.history.push('/');
    }

    return (
      <div>
        <h3>Edit Stream</h3>
        <StreamForm onSubmit={this.onSubmit} initialValues={_.pick(this.props.stream, 'title', 'description')} />
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id],
    currentUserId: state.auth.userId
  }
}

export default connect(mapStateToProps, { fetchStream, editStream })(StreamEdit);