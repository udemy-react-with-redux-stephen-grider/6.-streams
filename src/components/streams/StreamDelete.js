import React from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import { fetchStream, deleteStream } from '../../actions';
import Modal from '../Modal';
import Spinner from '../Spinner';

class StreamDelete extends React.Component {
  onDelete = () => {
    const { id } = this.props.match.params;
    this.props.deleteStream(id)
  }

  onDismiss = () => {
    this.props.history.push('/')
  }

  renderActionsModal = () => {
    return (
      < React.Fragment >
        <button onClick={this.onDelete} className='ui button negative'>Delete</button>
        <Link to='/' className='ui button'>Cancel</Link>
      </React.Fragment >
    )
  }

  renderContentModal = () => {
    return (
      <div>
        <p>Are you sure you want to delete this stream with title: <strong>{this.props.stream.title}</strong></p>
      </div>
    )
  }

  componentDidMount = () => {
    this.props.fetchStream(this.props.match.params.id)
  }

  render() {
    if (!this.props.stream) {
      return (<Spinner />)
    }

    return (
      <div>
        Stream delete
        <Modal
          title="Delete Stream"
          content={this.renderContentModal()}
          actions={this.renderActionsModal()}
          onDismiss={() => this.onDismiss}
        />
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id]
  }
}

export default connect(mapStateToProps, { fetchStream, deleteStream })(StreamDelete);