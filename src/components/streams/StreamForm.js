import React from 'react';
import { Field, reduxForm } from 'redux-form';

class StreamForm extends React.Component {
  renderErrors = ({ error, touched }) => {
    if (touched && error) {
      return (
        <div className='ui error message'>{error}</div>
      )
    }
  }

  renderInput = ({ input, label, meta }) => {
    return (
      <div className='field'>
        <label>{label}</label>
        <input  {...input} autoComplete='off' />
        {this.renderErrors(meta)}
      </div>
    )
  }

  onSubmit = (formValues) => {
    console.log(formValues);
    this.props.onSubmit(formValues)
  }

  render() {
    return (
      <form className='ui form error' onSubmit={this.props.handleSubmit(this.onSubmit)}>
        <Field name="title" component={this.renderInput} type='text' label='Enter Title' />
        <Field name="description" component={this.renderInput} type='text' label='Enter Description' />
        <button className='button green ui'>Submit</button>
      </form>
    )
  }
}

const validate = (formValues) => {
  const errors = {};

  if (!formValues.title) {
    errors.title = 'You must enter a title!'
  }

  if (!formValues.description) {
    errors.description = 'You must enter a descriptsion!'
  }

  return errors;
}

export default reduxForm({
  form: 'streamForm',
  validate
})(StreamForm);

/**
 * - StreamForm được wrap bởi ReduxForm
 * - Khi props được truyền từ component Cha xuống StreamForm sẽ ko đi trực tiếp mà thông qua ReduxForm
 * - key 'initialValues' chứa object các name field của ReduxForm thì sẽ được show lên các field input tương ứng
 * - Ở đây là props 'title' và 'description'
 *
 * - Lưu ý: khi sử dụng ReduxForm, luôn check props trong component cũng như các function validate, renderInput
 */